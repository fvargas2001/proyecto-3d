using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiculoMovimiento : MonoBehaviour
{
    public float velocidadHorizontal = 5f; // Velocidad de movimiento horizontal del jugador
    public float velocidadAdelante = 10f; // Velocidad de movimiento hacia adelante del jugador
    public float velocidadGiro = 100f; // Velocidad de giro del veh�culo


    void Update()
    {
        // Movimiento hacia adelante autom�tico
        transform.Translate(Vector3.forward * Time.deltaTime * velocidadAdelante);

        // Movimiento horizontal
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Time.deltaTime * velocidadHorizontal);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.deltaTime * velocidadHorizontal);
        }

        // Giro del veh�culo
        if (Input.GetKey(KeyCode.J))
        {
            transform.Rotate(Vector3.up, -velocidadGiro * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.K))
        {
            transform.Rotate(Vector3.up, velocidadGiro * Time.deltaTime);
        }
    }
}


